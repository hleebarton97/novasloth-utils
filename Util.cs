using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Audio;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {
    public static class Util {

        /////////////////////////////////////////////////////////////////
        // N U L L   U T I L S
        /////////////////////////////////////////////////////////////////

        // Check if Unity object is null or behaving like it should be null.
        public static bool IsNull (System.Object obj) => (obj == null || obj.Equals(null));

        /////////////////////////////////////////////////////////////////
        // T A G   U T I L S
        /////////////////////////////////////////////////////////////////

        public static class Tag {
            public static string MAIN_CAMERA = "MainCamera";
            public static string PLAYER = "Player";
            public static string ENEMY = "Enemy";
        }

        // Check for player
        public static bool IsPlayer (GameObject obj) => (obj.tag == Tag.PLAYER);
        public static bool IsPlayer (Collider collider) => IsPlayer(collider.gameObject);
        public static bool IsPlayer (Collider2D collider) => IsPlayer(collider.gameObject);

        // Check for enemy
        public static bool IsEnemy (GameObject obj) => (obj.tag == Tag.ENEMY);
        public static bool IsEnemy (Collider collider) => IsEnemy(collider.gameObject);
        public static bool IsEnemy (Collider2D collider) => IsEnemy(collider.gameObject);

        // Get main camera gameobject by tag.
        public static GameObject GetMainCameraByTag () => GameObject.FindGameObjectWithTag(Tag.MAIN_CAMERA);

        // Get specific parent by tag recursively.
        public static Transform GetParentByTag (Transform startingChild, string tag, int currentIndex, int maxIndex) {
            Transform parentOfChild = startingChild.parent;

            // Handle parent
            if (parentOfChild == null) {
                return null;
            } else if (parentOfChild.tag == tag) {
                return parentOfChild;
            }

            // Handle index
            if (currentIndex < maxIndex) { // maxIndex 0 = go to root
                currentIndex++;
            } else {
                return null;
            }

            return GetParentByTag(parentOfChild, tag, currentIndex, maxIndex);
        }

        /////////////////////////////////////////////////////////////////
        // T R A N S F O R M   U T I L S
        /////////////////////////////////////////////////////////////////

        // Test that children exist in a given transform and return the first child or null.
        public static Transform TryAndGetFirstChild (Transform transform) {
            if (transform.childCount > 0) {
                return transform.GetChild(0);
            }

            return null;
        }

        /////////////////////////////////////////////////////////////////
        // M A T H   &   P H Y S I C S   U T I L S
        /////////////////////////////////////////////////////////////////

        public static bool Vector3Equal (Vector3 vectorA, Vector3 vectorB) {
            return Vector3.SqrMagnitude(vectorA - vectorB) < 0.0001;
        }

        public static bool ReachedTarget (Vector3 targetVector, Vector3 movingVector, float marginOfError) {
            return Vector3.Distance(targetVector, movingVector) < marginOfError;
        }

        public static bool ReachedTarget (Vector3 targetVector, Vector3 movingVector) {
            return Vector3.Distance(targetVector, movingVector) < 0.001f;
        }

        /////////////////////////////////////////////////////////////////
        // N A V   U T I L S
        /////////////////////////////////////////////////////////////////

        // Reliably check if the provided NavMeshAgent has reached its destination.
        public static bool NavAgentReachedDestination (NavMeshAgent navAgent, float distance) {
            if (!navAgent.pathPending) { // Hasn't started calculating path to destination yet.
                if (navAgent.remainingDistance <= navAgent.stoppingDistance) { // Are we within stopping distance?
                    if (!navAgent.hasPath || navAgent.velocity.sqrMagnitude <= distance) { // We no longer have a path to a destination & not moving
                        return true;
                    }
                }
            }

            return false;
        }

        // Check that nav agent has reached it's target position.
        public static bool NavAgentReachedDestination (NavMeshAgent navAgent) {
            return NavAgentReachedDestination(navAgent, 0.0f);
        }

        /////////////////////////////////////////////////////////////////
        // T Y P E   U T I L S
        /////////////////////////////////////////////////////////////////

        // Enum enumerable helper method
        public static IEnumerable<T> GetEnumValues<T> () {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }

        // https://stackoverflow.com/a/10025398
        // https://gist.github.com/Virtlink/8722649
        public static class TypeSwitch {
            public static Switch<TSource> On<TSource> (TSource value) {
                return new Switch<TSource>(value);
            }

            public sealed class Switch<TSource> {
                private readonly TSource value;
                private bool handled = false;

                internal Switch (TSource value) {
                    this.value = value;
                }

                public Switch<TSource> Case<TTarget> (Action<TTarget> action)
                    where TTarget : TSource {

                    if (!this.handled && this.value is TTarget) {
                        action((TTarget)this.value);
                        this.handled = true;
                    }

                    return this;
                }

                public void Default (Action<TSource> action) {
                    if (!this.handled) {
                        action(this.value);
                    }
                }
            }
        }

        /////////////////////////////////////////////////////////////////
        // J S O N   U T I L S
        /////////////////////////////////////////////////////////////////

        // Json Helper class for serializing non-primative types such as arrays
        public static class Json {
            public static string WrapJsonInObject (string json) {
                return "{\"Elements\":" + json + "}";
            }

            public static T[] FromJson<T> (string json) {
                return JsonUtility.FromJson<Wrapper<T>>(json).Elements;
            }

            public static string ToJson<T> (T[] array) {
                Wrapper<T> wrapper = new Wrapper<T>();
                wrapper.Elements = array;

                return JsonUtility.ToJson(wrapper);
            }

            [Serializable]
            private class Wrapper<T> {
                public T[] Elements;
            }
        }

        /////////////////////////////////////////////////////////////////
        // A U D I O   U T I L S
        /////////////////////////////////////////////////////////////////

        // Create an audio source for audio events
        public static AudioSource SetupAudioSource (GameObject gameObject, AudioMixerGroup mixerGroup) {
            AudioSource audioSource = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
            audioSource.outputAudioMixerGroup = mixerGroup;

            return audioSource;
        }

        // Create an audio source for audio events, in master group
        public static AudioSource SetupAudioSource (GameObject gameObject) {
            AudioMixer mainMixer = Resources.Load<AudioMixer>("Mixers/Main");
            AudioMixerGroup masterGroup = mainMixer.FindMatchingGroups("Master")[0];

            if (IsNull(masterGroup)) {
                Debug.LogError("Util :: Master Main Mixer group cannot be found!");
                return null;
            }

            return SetupAudioSource(gameObject, masterGroup);
        }
    }
}
