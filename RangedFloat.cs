using System;

// Novasloth Games LLC
// Lee Barton
namespace Novasloth {

    [Serializable]
    public struct RangedFloat {
        public float minValue;
        public float maxValue;
    }
}
